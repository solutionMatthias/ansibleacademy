# Ansible Academy

## Install Ansible
Follow the instructions on the [setup-page](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

You should do the installation with pip3. 

## Install VirtualBox
Follow the instructions on the [setup-page](https://www.virtualbox.org/wiki/Downloads)

## Install Vagrant
Follow the instruction on the [setup-page](https://www.vagrantup.com/docs/installation/)

### start the vagrant machines
#### Elastic
```bash
cd vagrant/elastic
```

```bash
vagrant up
```

#### Gitlab
```bash
cd vagrant/gitlab
```

```bash
vagrant up
```

Wait till both vagrant machines are up and running

### Install requirements
```bash
ansible-galaxy install -r requirements.yml
```

### edit your /etc/hosts - file
add the following lines to your `/etc/hosts` file (sudo required)

```
192.168.60.163  gitlab.test
192.168.60.164  elastic.test
```

### Run ansible
```bash
ansible-playbook playbooks/playbook.yml -i inventories
```

### visit your machines

[gitlab-frontend](https://gitlab.test)

[kibana-frontend](http://elastic.test:5601)


### Play around with ansible
